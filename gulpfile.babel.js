'use strict';

import gulp from 'gulp';
import paths from './config.json';

const requireDir = require('require-dir');

requireDir('./tasks/');

export { paths };

export const development = gulp.series('clean',
	gulp.parallel([
		'templates',
		'styles',
		'scripts',
		'images',
		'webp',
		'sprites',
		'fonts',
		'favicons'
	]),
	gulp.parallel('serve'));

export const prod = gulp.series('clean',
	gulp.series([
		'templates',
		'styles',
		'scripts',
		'images',
		'webp',
		'sprites',
		'fonts',
		'favicons',
		'archive'
	]));

export const cms = gulp.series(
	gulp.parallel([
		'styles',
		'scripts',
		'images',
		'webp',
		'sprites',
		'fonts'
	]),
	gulp.parallel('serve'));

export const cmsProd = gulp.series(
	gulp.parallel([
		'styles',
		'scripts',
		'images',
		'webp',
		'sprites',
		'fonts'
	]));

export default development;
