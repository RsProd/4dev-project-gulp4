'use strict';

import { paths } from '../gulpfile.babel';
import gulp from 'gulp';
import svg from 'gulp-svg-sprite';
import debug from 'gulp-debug';
import browsersync from 'browser-sync';
import gulpif from 'gulp-if';
import yargs from 'yargs';

const argv = yargs.argv,
	cms = !!argv.cms;

gulp.task('sprites', () => {
	return gulp.src(paths.sprites.src)
		.pipe(svg({
			shape: {
				dest: 'source'
			},
			mode: {
				stack: {
					sprite: '../sprite.svg'
				}
			}
		}))
		.pipe(gulp.dest(gulpif(cms, paths.sprites.cms, paths.sprites.dist)))
		.pipe(debug({
			'title': 'Sprites'
		}))
		.on('end', browsersync.reload);
});
