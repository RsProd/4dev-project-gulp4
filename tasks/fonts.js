'use strict';

import { paths } from '../gulpfile.babel';
import gulp from 'gulp';
import debug from 'gulp-debug';
import gulpif from 'gulp-if';
import yargs from 'yargs';

const argv = yargs.argv,
	cms = !!argv.cms;

gulp.task('fonts', () => {
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(gulpif(cms, paths.fonts.cms, paths.fonts.dist)))
		.pipe(debug({
			'title': 'Fonts'
		}));
});
